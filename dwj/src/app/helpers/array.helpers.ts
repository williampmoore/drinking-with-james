export class ArrayHelpers {

  public static SHUFFLE<T>(array: T[]): T[] {
    return array.sort(() => (Math.random() > .5) ? 1 : -1);
  }

}