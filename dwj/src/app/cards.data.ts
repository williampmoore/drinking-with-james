import { ICard } from './components/card/card.interface';
import { ArrayHelpers } from './helpers/array.helpers';

export class CardsData {

  private static CARDS: any[] = [
    { imageUrl: 'card-front-dennis', cardTitle: 'Question', questionText: 'What property could be used to describe James personality', questionAnswer: [{ text: 'Sensitive', isCorrect: true }, { text: 'Athletic', isCorrect: false }, { text: 'Pathetic', isCorrect: false }], state: 'default', type: 'question' },
    { imageUrl: 'card-front-beer-1', cardTitle: 'DRINK', textDetail: 'Everyone drink', state: 'default', type: 'text' },
    { imageUrl: 'card-front-beer-2', cardTitle: 'WATERFALL', state: 'default', type: 'text', textDetail: 'Waterfall starting from the player on your left' },
    { imageUrl: 'card-front-beer-3', cardTitle: 'DRINK', state: 'default', type: 'text', textDetail: 'You drink' },
    { imageUrl: 'card-front-vodka', cardTitle: 'DRINK', state: 'default', type: 'text', textDetail: 'Everyone drink' },
    { imageUrl: 'card-front-hair', cardTitle: 'Question', state: 'default', type: 'question', questionText: 'James aspired to many things at school.  Which is not an item he wanted from his yearbook?', questionAnswer: [{ text: 'B a gangster', isCorrect: true }, { text: 'Make Africa rich', isCorrect: true }, { text: 'buy loads of houses cars planes boats', isCorrect: true }] },
    { imageUrl: 'card-front-captin', cardTitle: 'Put your hands in the air', state: 'default', type: 'text', textDetail: 'Last person to put their hands up has to drink' },
    { imageUrl: 'card-front-cig', cardTitle: 'VOTE', state: 'default', type: 'text', textDetail: 'Everyone vote for a player to drink' },
    { imageUrl: 'card-front-dennis', cardTitle: 'Tallest player', state: 'default', type: 'text', textDetail: 'Must stand up for the rest of the round' },
    { imageUrl: 'card-front-dance', cardTitle: 'Ginger players...', state: 'default', type: 'text', textDetail: '...must drink' },
    { imageUrl: 'card-front-eyes', cardTitle: 'Game', state: 'default', type: 'text', textDetail: 'Going left everyone must name a type of car James has owned... first to fail drinks.' },
    { imageUrl: 'card-front-gun', cardTitle: 'Question', state: 'default', type: 'question', questionText: 'Many teachers mugged James off at school.  Which of these teachers said that you put your life at risk by going in his car?', questionAnswer: [{ text: 'Mr Sharp', isCorrect: true }, { text: 'Mrs Prouse', isCorrect: false }, { text: 'Dr Ingham', isCorrect: false }] },
    { imageUrl: 'card-front-jamie', cardTitle: 'Competition', state: 'default', type: 'text', textDetail: 'Whoever can plank for longest chooses a player to drink' },
    { imageUrl: 'card-front-mouth', cardTitle: 'RULE', state: 'default', type: 'text', textDetail: 'Make a new rule' },
    { imageUrl: 'card-front-pants', cardTitle: 'Pick a drinking buddy', state: 'default', type: 'text', textDetail: 'They must drink when you do' },
    { imageUrl: 'card-front-pizza', cardTitle: 'Game', state: 'default', type: 'text', textDetail: 'Going right everyone must name a country which James has visited' },
    { imageUrl: 'card-front-ryan', cardTitle: 'Eyes', state: 'default', type: 'text', textDetail: 'The next player to make eye contact with James has to down their drink' },
    { imageUrl: 'card-front-sleep', cardTitle: 'Question Master', state: 'default', type: 'text', textDetail: 'You are the question master... next player to answer your question has to drink' },
    { imageUrl: 'card-front-swear', cardTitle: 'DRINK', state: 'default', type: 'text', textDetail: 'You and James must drink.' },
  ];

  private static SORTED_CARDS: any[] = ArrayHelpers.SHUFFLE(CardsData.CARDS);

  private static COUNTER: number = 0;

  public static get NEXT_CARD(): ICard {
    if (CardsData.COUNTER >= CardsData.CARDS.length) {
      CardsData.RESET();
      throw new Error();
    }

    return CardsData.SORTED_CARDS[CardsData.COUNTER++];
  }

  private static RESET(): void {
    CardsData.CARDS.forEach(x => x.state = 'default');
    CardsData.SORTED_CARDS = ArrayHelpers.SHUFFLE(CardsData.CARDS);
    CardsData.COUNTER = 0;
  }

}