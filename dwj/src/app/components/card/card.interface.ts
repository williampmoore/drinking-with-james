import { CardState } from './card-state.data';
import { CardType } from './card-type.data';

export interface ICard {

  imageUrl: string;
  cardTitle: string;
  state: CardState;
  type: CardType;

}