import { Component, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'; 

import { ICard } from './card.interface';
import { CardState } from './card-state.data';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  animations: [
    trigger('cardFlip', [
      state('default', style({
        transform: 'none'
      })),
      state('flipped', style({
        transform: 'rotateY(180deg)'
      })),
      transition('default => flipped', [
        animate('400ms')
      ]),
      transition('flipped => default', [
        animate('0ms')
      ])
    ])
  ]
})
export class CardComponent {

  private static BACK_IMAGE_URL: string = 'https://williampmoore.gitlab.io/dwj/assets/images/card-back.jpg';

  @Input()
  private data: ICard;

  constructor() { }

  public onClick(): void {
    this.data.state = "flipped";
  }

  public get imageUrl(): string {
    return `https://williampmoore.gitlab.io/dwj/assets/images/${this.data.imageUrl}.jpg`;
  }

  public get backImageUrl(): string {
    return CardComponent.BACK_IMAGE_URL;
  }

  public get flipState(): CardState {
    return this.data.state;
  }

  public get card(): ICard {
    return this.data;
  }

}
