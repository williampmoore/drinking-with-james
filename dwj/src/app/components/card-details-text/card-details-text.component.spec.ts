import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailsTextComponent } from './card-details-text.component';

describe('CardDetailsTextComponent', () => {
  let component: CardDetailsTextComponent;
  let fixture: ComponentFixture<CardDetailsTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDetailsTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetailsTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
