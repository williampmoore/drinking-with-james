import { Component, Input } from '@angular/core';
import { ITextCard } from './text-card.interface';

@Component({
  selector: 'app-card-details-text',
  templateUrl: './card-details-text.component.html',
  styleUrls: ['./card-details-text.component.css']
})
export class CardDetailsTextComponent {

  @Input()
  private data: ITextCard;

  constructor() { }

  public get textDetail(): string {
    return this.data.textDetail;
  }

}
