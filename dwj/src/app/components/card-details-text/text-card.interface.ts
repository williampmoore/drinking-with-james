import { ICard } from '../card/card.interface';

export interface ITextCard extends ICard {

  textDetail: string;
  
}