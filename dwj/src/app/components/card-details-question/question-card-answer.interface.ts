export interface IQuestionCardAnswer {

  text: string;
  isCorrect: boolean;
  
}