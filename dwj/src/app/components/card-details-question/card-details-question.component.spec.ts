import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailsQuestionComponent } from './card-details-question.component';

describe('CardDetailsQuestionComponent', () => {
  let component: CardDetailsQuestionComponent;
  let fixture: ComponentFixture<CardDetailsQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDetailsQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetailsQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
