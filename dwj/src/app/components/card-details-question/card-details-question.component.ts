import { Component, OnInit, Input } from '@angular/core';
import { IQuestionCard } from './question-card.interface';
import { IQuestionCardAnswer } from './question-card-answer.interface';
import { ArrayHelpers } from '../../helpers/array.helpers';

@Component({
  selector: 'app-card-details-question',
  templateUrl: './card-details-question.component.html',
  styleUrls: ['./card-details-question.component.css']
})
export class CardDetailsQuestionComponent implements OnInit {

  @Input()
  private data: IQuestionCard;

  public answers: IQuestionCardAnswer[];
  public displayQuestion: boolean = true;
  public displayAnswer: boolean = false;
  public answerMessage: string;

  constructor() { }

  public ngOnInit(): void {
    this.answers = ArrayHelpers.SHUFFLE(this.data.questionAnswer);
  }

  public get questionText(): string {
    return this.data.questionText;
  }

  public get correctAnswerIndex(): number {
    return this.answers.findIndex(x => x.isCorrect);
  }

  public onClick(answer: IQuestionCardAnswer): void {
    this.displayQuestion = false;
    this.displayAnswer = true;

    this.answerMessage = this.getAnswerMessage(answer);
  }

  private getAnswerMessage(answer: IQuestionCardAnswer): string {
    if (answer.isCorrect) {
      return `${answer.text} is correct!  James drink!`;
    }

    return `Incorrect :(.  Correct answer was ${this.correctAnswer}.  You drink!`;
  }

  private get correctAnswer(): string {
    return this.answers.find(x => x.isCorrect).text;
  }

}
