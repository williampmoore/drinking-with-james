import { ICard } from '../card/card.interface';
import { IQuestionCardAnswer } from './question-card-answer.interface';

export interface IQuestionCard extends ICard {

  questionText: string;
  questionAnswer: IQuestionCardAnswer[];
  
}
