import { Component, Input } from '@angular/core';
import { ICard } from '../card/card.interface';
import { CardType } from '../card/card-type.data';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.css']
})
export class CardDetailsComponent {

  @Input()
  private data: ICard;

  constructor() { }

  public get title(): string {
    return this.data.cardTitle;
  }

  public get type(): CardType {
    return this.data.type;
  }

  public get card(): ICard {
    return this.data;
  }

}
