import { Component, OnInit } from '@angular/core';
import { ICard } from './components/card/card.interface';
import { CardsData } from './cards.data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private _card: ICard;

  public showDrunkPirateLink: boolean;

  title = 'dwj';

  public get card(): ICard {
    return this._card;
  }

  public ngOnInit(): void {
    this._setNextCard();
  }

  public onNextClick(): void {
    this._setNextCard();
  }

  private _setNextCard(): void {
    try {
      this._card = CardsData.NEXT_CARD;
    } catch (ex) {
      this.showDrunkPirateLink = true;
    }
  }

}
