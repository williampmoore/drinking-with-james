import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import {MatButtonModule} from '@angular/material/button';

import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { CardDetailsTextComponent } from './components/card-details-text/card-details-text.component';
import { CardDetailsComponent } from './components/card-details/card-details.component';
import { CardDetailsQuestionComponent } from './components/card-details-question/card-details-question.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CardDetailsTextComponent,
    CardDetailsComponent,
    CardDetailsQuestionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
